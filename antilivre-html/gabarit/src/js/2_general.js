// Propeller
//
let propeller1 = new Propeller('.rotation-possible', {inertia: 0.99, speed: 0});
let propeller2 = new Propeller('.rotation-depart', {inertia: 0.99, speed: 10});

//
// rotation
//

const toggleDroite = () => document.querySelectorAll('.page__contenu--droite')
  .forEach(label => label.classList.toggle('rotate'))

document.querySelector('#rotation-droite').addEventListener('change', toggleDroite);

const toggleGauche = () => document.querySelectorAll('.page__contenu--gauche')
  .forEach(label => label.classList.toggle('rotate'))

document.querySelector('#rotation-gauche').addEventListener('change', toggleGauche);

//
// scroll
// Source : https://codepen.io/rebosante/pen/eENYBv

var elemGauche = document.querySelector(".page__contenu--gauche");
var elemDroite = document.querySelector(".page__contenu--droite");

// var topPos = elem.offsetTop

document.querySelector('#pied-gauche').onclick = function () {
  scrollTo(elemGauche, elemGauche.scrollHeight, 600);
}
document.querySelector('#sommet-gauche').onclick = function () {
  scrollTo(elemGauche, 0, 600);
}
document.querySelector('#bas-gauche').onclick = function () {
  scrollTo(elemGauche, elemGauche.scrollTop + 300, 200);
}
document.querySelector('#haut-gauche').onclick = function () {
  scrollTo(elemGauche, elemGauche.scrollTop - 300, 200);
}

document.querySelector('#pied-droite').onclick = function () {
  scrollTo(elemDroite, elemDroite.scrollHeight, 600);
}
document.querySelector('#sommet-droite').onclick = function () {
  scrollTo(elemDroite, 0, 600);
}
document.querySelector('#bas-droite').onclick = function () {
  scrollTo(elemDroite, elemDroite.scrollTop + 300, 200);
}
document.querySelector('#haut-droite').onclick = function () {
  scrollTo(elemDroite, elemDroite.scrollTop - 300, 200);
}

function scrollTo(element, to, duration) {
    var start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

    var animateScroll = function(){
        currentTime += increment;
        var val = Math.easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if(currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}

//t = current time
//b = start value
//c = change in value
//d = duration
Math.easeInOutQuad = function (t, b, c, d) {
	t /= d/2;
	if (t < 1) return c/2*t*t + b;
	t--;
	return -c/2 * (t*(t-2) - 1) + b;
};

//
// Fullscreen
//
document.querySelector('#plein-gauche').onclick = function () {
  document.querySelector('.conteneur--grille').classList.toggle('conteneur--grille-plein-gauche');
  document.querySelector('.page__options--droite').classList.toggle('none');
  document.querySelector('.page__contenu--droite').classList.toggle('none');
}

document.querySelector('#plein-droite').onclick = function () {
  document.querySelector('.conteneur--grille').classList.toggle('conteneur--grille-plein-droite');
  document.querySelector('.page__options--gauche').classList.toggle('none');
  document.querySelector('.page__contenu--gauche').classList.toggle('none');
}
