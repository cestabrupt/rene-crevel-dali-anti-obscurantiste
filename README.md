# ~/ABRÜPT/RENÉ CREVEL/DALI ANTI-OBSCURANTISTE/*

La [page de ce livre](https://abrupt.ch/rene-crevel/dali-anti-obscurantiste/) sur le réseau.

## Sur le livre

Dalí n’est pas un rhinocéros, il est la surréalité, et la surréalité est la « réalité rendue à son devenir, réalité se dépassant elle-même et destinée à se dépasser sans cesse elle-même. L’homme qui doit, selon l’expression familière, savoir sortir de lui-même, comment y parviendrait-il, sans faire sortir les choses d’elles-mêmes ? »

Un *livre double face* pour se précipiter au cœur de la surréalité. Deux faces, deux textes : *Dalí ou l’anti-obscurantisme* et *Nouvelles vues sur Dalí et l’obscurantisme*.

## Sur l'auteur

René Crevel, de l’écriture révoltée à la casse du roman, l’irraison surréaliste, et sans détour, l’inventeur des sommeils. De Babylone à la mort difficile. Dégoût. 

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
